import sys

import cv2
import imutils

from anpr import PyImageSearchANPR

object_detector = cv2.createBackgroundSubtractorMOG2(history=150, varThreshold=20, 	detectShadows=False)

ratio = (2, 3)


def cleanup_text(text):
    # strip out non-ASCII text so we can draw the text on the image
    # using OpenCV
    return "".join([c if ord(c) < 128 else "" for c in text]).strip()


def process_video(frames):
    anpr = PyImageSearchANPR(False)
    while frames.isOpened():
        ret, frame = frames.read()
        if not ret:
            break
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (5, 5), 0)
        gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, (5, 5))
        mask = object_detector.apply(gray)
        contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours = sorted(contours, key=cv2.contourArea, reverse=True)[:5]
        mask = cv2.dilate(mask, (7, 7), iterations=5)
        # mask = cv2.threshold(mask, 254, 255, cv2.THRESH_BINARY)[1]
        # multiple contours
        # for cnt in contours:
        #     x, y, w, h = cv2.boundingRect(cnt)
        #     area = cv2.contourArea(cnt)
        #     ar = w / float(h)
        #     if ratio[0] <= ar <= ratio[1] and area > 100:
        #         x, y, w, h = cv2.boundingRect(cnt)
        #         cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 255, 255), cv2.FILLED)
        # one contour/frame,
        if len(contours) >= 1:
            cnt = contours[0]
            x, y, w, h = cv2.boundingRect(cnt)
            roi = frame[y:y + h, x:x + w]
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0))
            image = imutils.resize(roi, width=600)
            (lpText, lpCnt) = anpr.find_and_ocr(image, 7, False)
            if lpText is not None and lpCnt is not None:
                cv2.putText(frame, cleanup_text(lpText), (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 255, 0), 2)
                print("[INFO] {}".format(lpText))
            # cv2.imshow("ROI", roi)
        # cv2.imshow('Mask', mask)
        # cv2.imshow('Original', frame)
        if cv2.waitKey(25) & 0xFF == ord('q'):
            break
    frames.release()
    cv2.destroyAllWindows()


def main():
    video = cv2.VideoCapture(sys.argv[1])
    process_video(video)


if __name__ == '__main__':
    main()
