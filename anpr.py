# import the necessary packages
from skimage.segmentation import clear_border
import pytesseract
import numpy as np
import imutils
import cv2


class PyImageSearchANPR:
    def __init__(self, minAR=3.5, maxAR=5.5, debug=False):
        # store the minimum and maximum rectangular aspect ratio
        # values along with whether or not we are in debug mode
        self.minAR = minAR
        self.maxAR = maxAR
        self.debug = debug

    def debug_imshow(self, title, image, waitKey=False):
        # check to see if we are in debug mode, and if so, show the
        # image with the supplied title
        if self.debug:
            cv2.imshow(title, image)
            # check to see if we should wait for a keypress
            if waitKey:
                cv2.waitKey(0)

    def locate_license_plate_candidates(self, gray, keep=5):
        # perform a blackhat morphological operation that will allow
        # us to reveal dark regions (i.e., text) on light backgrounds
        # (i.e., the license plate itself)
        rectKern = cv2.getStructuringElement(cv2.MORPH_RECT, (13, 5))
        blackhat = cv2.morphologyEx(gray, cv2.MORPH_BLACKHAT, rectKern)
        self.debug_imshow("Blackhat", blackhat)
        # next, find regions in the image that are light
        # squareKern = cv2.getStructuringElement(cv2.MORPH_RECT, (13, 5))
        light = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, rectKern)
        threshold, light = cv2.threshold(light, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        self.debug_imshow("Light Regions", light)
        # compute the Scharr gradient representation of the blackhat
        # image in the x-direction and then scale the result back to
        # the range [0, 255]
        #  grad_x = cv2.Sobel(blackhat, ddepth=cv2.CV_32F, dx=1, dy=0, ksize=-1)
        grad_x = cv2.Scharr(blackhat, ddepth=cv2.CV_32F, dx=1, dy=0)  # ksize=-1)
        grad_x = np.absolute(grad_x)
        (minVal, maxVal) = (np.min(grad_x), np.max(grad_x))
        grad_x = 255 * ((grad_x - minVal) / (maxVal - minVal))
        grad_x = grad_x.astype("uint8")

        ######################
        grad_y = cv2.Scharr(blackhat, ddepth=cv2.CV_32F, dx=0, dy=1)  # ksize=-1)
        grad_y = np.absolute(grad_y)
        (minVal, maxVal) = (np.min(grad_y), np.max(grad_y))
        grad_y = 255 * ((grad_y - minVal) / (maxVal - minVal))
        grad_y = grad_y.astype("uint8")
        combined = cv2.addWeighted(grad_x, 0.5, grad_y, 0.5, 0)
        edges = cv2.Canny(blackhat, threshold, 255)
        self.debug_imshow("Canny", edges)
        self.debug_imshow("Combined", combined)
        # self.debug_imshow("Scharr", grad_x)
        ######################
        grad_x = combined
        # cv2.bilateralFilter()
        # cv2.medianBlur()
        # blur the gradient representation, applying a closing
        # operation, and threshold the image using Otsu's method
        grad_x = cv2.GaussianBlur(grad_x, (3, 3), 0)
        grad_x = cv2.morphologyEx(grad_x, cv2.MORPH_CLOSE, rectKern)
        thresh = cv2.threshold(grad_x, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
        # self.debug_imshow("Grad Thresh", thresh)
        # perform a series of erosions and dilations to clean up the
        # thresholded image
        thresh = cv2.erode(thresh, None, iterations=2)
        thresh = cv2.dilate(thresh, None, iterations=2)
        self.debug_imshow("Grad Erode/Dilate after trash", thresh)

        # take the bitwise AND between the threshold result and the
        # light regions of the image
        thresh = cv2.bitwise_and(thresh, light)  # , mask=light)
        self.debug_imshow("Logical", thresh)
        thresh = cv2.bitwise_and(thresh, thresh, mask=light)
        self.debug_imshow("Masked", thresh)
        thresh = cv2.dilate(thresh, None, iterations=2)
        thresh = cv2.erode(thresh, None, iterations=1)
        self.debug_imshow("Final", thresh, waitKey=True)

        # find contours in the thresholded image and sort them by
        # their size in descending order, keeping only the largest
        # ones
        cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        blank = np.zeros(gray.shape, dtype="uint8")
        cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:keep]
        cv2.drawContours(blank, cnts, -1, (255, 255, 255), 1)
        self.debug_imshow("Contours", blank)
        # return the list of contours
        return cnts

    def locate_license_plate(self, gray, candidates, clearBorder=False):
        # initialize the license plate contour and ROI
        lpCnt = None
        roi = None
        # loop over the license plate candidate contours
        i = 0
        for c in candidates:
            # compute the bounding box of the contour and then use
            # the bounding box to derive the aspect ratio
            (x, y, w, h) = cv2.boundingRect(c)
            ar = w / float(h)
            # check to see if the aspect ratio is rectangular
            blank = np.zeros(gray.shape, dtype="uint8")
            cv2.drawContours(blank, c, -1, (255, 255, 255), 1)
            # cv2.addText(blank, str(i), (0, 0), (255, 255, 255))
            self.debug_imshow(f"{i}  {ar}", blank, waitKey=True)
            i += 1
            if self.minAR <= ar <= self.maxAR:
                # store the license plate contour and extract the
                # license plate from the grayscale image and then
                # threshold it
                lpCnt = c
                licensePlate = gray[y:y + h, x:x + w]
                roi = cv2.threshold(licensePlate, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
                # pixels touching the border of the image
                # (which typically, not but always, indicates noise)
                if clearBorder:
                    roi = clear_border(roi)
                # display any debugging information and then break
                # from the loop early since we have found the license
                # plate region
                # self.debug_imshow("License Plate", licensePlate)
                # self.debug_imshow("ROI", roi, waitKey=True)
                break
        # return a 2-tuple of the license plate ROI and the contour
        # associated with it
        return roi, lpCnt

    def build_tesseract_options(self, psm=7):
        # tell Tesseract to only OCR alphanumeric characters
        alphanumeric = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        options = "-c tessedit_char_whitelist={}".format(alphanumeric)
        # set the PSM mode
        options += " --psm {}".format(psm)
        # return the built options string
        return options

    def find_and_ocr(self, image, psm=7, clearBorder=False):
        # initialize the license plate text
        lpText = None
        # convert the input image to grayscale, locate all candidate
        # license plate regions in the image, and then process the
        # candidates, leaving us with the *actual* license plate
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        candidates = self.locate_license_plate_candidates(gray)
        (lp, lpCnt) = self.locate_license_plate(gray, candidates, clearBorder=clearBorder)
        # only OCR the license plate if the license plate ROI is not
        # empty
        if lp is not None:
            # OCR the license plate
            # image = cv2.copyMakeBorder(src, top, bottom, left, right, borderType)
            border = 30
            lp = cv2.copyMakeBorder(lp, border, border, border, border, cv2.BORDER_CONSTANT)
            kernel = np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]])
            lp = cv2.filter2D(lp, -1, kernel)
            kernel = np.ones((1, 1), np.uint8)
            lp = cv2.dilate(lp, kernel, iterations=1)
            lp = cv2.erode(lp, kernel, iterations=1)
            lp = cv2.bitwise_not(lp)
            self.debug_imshow("Final license plate", lp, True)
            options = self.build_tesseract_options(psm=psm)
            lpText = pytesseract.image_to_string(lp, config=options)
        # self.debug_imshow("License Plate", lp)
        # return a 2-tuple of the OCR'd license plate text along with
        # the contour associated with the license plate region
        return lpText, lpCnt
